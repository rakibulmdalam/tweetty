from elasticsearch import Elasticsearch


class Elasticity:

    def __init__(self, address):
        try:
            self.ES = Elasticsearch(address, timeout=30, max_retries=10, retry_on_timeout=True)
        except:
            print('Error: Connection to Elastic server failed ')
    
    def save(self, data, ind, doc_type):
        try:
            response = self.ES.index(index=ind, doc_type='general', body=data)
            return response
        except:
            print('Error: could not save data on Elastic cloud')
            return False

    def fetch(self):
        pass

    def update(self):
        pass

    def delete(self):
        pass