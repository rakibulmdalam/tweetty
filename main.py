from twitter_listener import TwitterListener
from elasticity import Elasticity
import argparse
import json
import datetime as dt
import csv


def input():
    ap = argparse.ArgumentParser()
    ap.add_argument("-c", "--config", help="path to config file", required=True)
    ap.add_argument("-u", "--users", help="path to user list file", required=True)
    args = vars(ap.parse_args())
    return args

if __name__ == "__main__":
    args = input()
    print(args)
    try:
        with open(args['config'], 'r') as config_file:
            configs = json.load(config_file)
    except:
        print('Error: Config file not found on specified path')
        exit(0)
    
    try:
        with open(args['users'], 'rb') as f:
            reader = csv.reader(f)
            users = list(reader)
            # flattening users list
            users = [item for sublist in users for item in sublist]

    except:
        print('Error: user list file not found on specified path')
        exit(0)
    
    for config in configs:
        print('calling twitty')
        twitty = TwitterListener(config)
        es = Elasticity(config['elastic'])

        for user in users:
            index = user
            doc_type = 'general'
            since_id = 0
            tweets = twitty.collect(user)
            if len(tweets) > 0:
                for tweet in tweets:
                    created_at = tweet._json['created_at']
                    created_at_date = dt.datetime.strptime(created_at, '%a %b %d %H:%M:%S +0000 %Y')
                    created_at_timestamp = created_at_date.timestamp()
                    tweet._json['created_at_timestamp'] = created_at_timestamp
                    tweet._json['digidemo_target'] = index
                    es.save(tweet._json, "["+index.lower()+"]", doc_type)
            else:
                print('No tweets found for user {}'.format(user))

