import tweepy
import json

class TwitterListener:

    def __init__(self, config):
        self.CONSUMER_KEY = config['twitter_consumer_key']
        self.CONSUMER_SECRET = config['twitter_consumer_secret']
        self.ACCESS_TOKEN_KEY = config['twitter_access_token_key']
        self.ACCESS_TOKEN_SECRET = config['twitter_access_token_secret']
        #self.load_config(config)
        #self.es = Elasticity(self.ELASTIC_ADDRESS)
        self.oauth()

    def oauth(self):
        try:
            auth = tweepy.OAuthHandler(self.CONSUMER_KEY, self.CONSUMER_SECRET)
            auth.set_access_token(self.ACCESS_TOKEN_KEY, self.ACCESS_TOKEN_SECRET)
            self.api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
        except:
            print('Error: Twitter authentication failed')
            exit(0)

    def collect(self, user, since_id=False, tweet_mode='extended'):
        if since_id:
            try:
                return self.api.user_timeline(user, since_id=since_id, tweet_mode=tweet_mode)
            except:
                print('Could not retieve data')
                return []
            
        else:
            try:
                return self.api.user_timeline(user, count=20, tweet_mode=tweet_mode)
            except:
                print('Could not retieve data')
                return []
        
